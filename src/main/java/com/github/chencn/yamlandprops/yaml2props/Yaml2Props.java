package com.github.chencn.yamlandprops.yaml2props;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.error.YAMLException;
import org.yaml.snakeyaml.reader.UnicodeReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.Reader;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * 将yaml构建成一颗树，从根结点到叶子结点为key，叶子结点的值为value
 * @author xqchen
 */
public class Yaml2Props {

    TreeMap<String, Map<String, Object>> config;

    public Yaml2Props(String contents) {
        Yaml yaml = new Yaml();
        this.config = yaml.loadAs(contents, TreeMap.class);
    }

    public Yaml2Props(Reader reader) {
        Yaml yaml = new Yaml();
        this.config = yaml.loadAs(reader, TreeMap.class);
    }

    public static Yaml2Props fromContent(String content) {
        return new Yaml2Props(content);
    }

    public static Yaml2Props fromFile(File f) {
        try (Reader reader = new UnicodeReader(new FileInputStream(f))) {
            return new Yaml2Props(reader);
        } catch (Exception e) {
            throw new YAMLException(f.getPath() + ", " + e.getMessage(), e);
        }
    }

    public String toContent() {
        StringBuilder sb = new StringBuilder();

        Map<String, Object> map = toMap();
        for (final Map.Entry<String, Object> entry : map.entrySet()) {
            sb.append(String.format("%s=%s%n", entry.getKey(), entry.getValue()));
        }
        return sb.toString();
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new TreeMap<>(Comparator.naturalOrder());
        for (final Map.Entry<String, Map<String, Object>> entry : config.entrySet()) {
            getValue(map, entry.getKey(), entry.getValue());
        }
        return map;
    }

    private void getValue(Map<String, Object> resultMap, final String key, final Object o) {
        if (o instanceof Map) {
            Map<String, Object> map = (Map<String, Object>) o;
            for (final String mapKey : map.keySet()) {
                if (map.get(mapKey) instanceof Map) {
                    getValue(resultMap, String.format("%s.%s", key, mapKey), map.get(mapKey));
                } else {
                    // 叶子结点
                    resultMap.put(String.format("%s.%s", key, mapKey), map.get(mapKey));
                }
            }
        } else {
            resultMap.put(key, o);
        }
    }
}
