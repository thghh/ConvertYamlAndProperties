package com.github.chencn.yamlandprops.props2yaml;

import java.io.*;
import java.nio.file.Path;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author xqchen
 */
public class Props2Yaml {

    private final static Logger LOG = Logger.getLogger(Props2Yaml.class.getName());

    private final Properties properties = new Properties();

    Props2Yaml(String source) {
        try {
            properties.load(new StringReader(source));
        } catch (IOException e) {
            reportError(e);
        }
    }

    Props2Yaml(File file) {
        try (InputStream input = new FileInputStream(file)) {
            properties.load(input);
        } catch (IOException e) {
            reportError(e);
        }
    }

    Props2Yaml(Map<String, Object> map) {
        this.properties.putAll(map);
    }

    public static Props2Yaml fromContent(String content) {
        return new Props2Yaml(content);
    }

    public static Props2Yaml fromFile(File file) {
        return new Props2Yaml(file);
    }

    public static Props2Yaml fromFile(Path path) {
        return new Props2Yaml(path.toFile());
    }

    public static Props2Yaml fromMap(Map<String, Object> map) {
        return new Props2Yaml(map);
    }

    public String convert(boolean useNumericKeysAsArrayIndexes) {
        PropertyTree tree = new TreeBuilder(properties, useNumericKeysAsArrayIndexes).build();
        tree = new ArrayProcessor(tree).apply();
        return tree.toYaml();
    }

    public String convert() {
        return convert(true);
    }

    private void reportError(IOException e) {
        LOG.log(Level.SEVERE, "Conversion failed", e);
    }

}

