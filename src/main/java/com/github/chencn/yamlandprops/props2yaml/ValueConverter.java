package com.github.chencn.yamlandprops.props2yaml;

import com.github.chencn.yamlandprops.util.NumberUtil;

import java.util.stream.IntStream;

/**
 * @author xqchen
 */
class ValueConverter {
    public final static String TRUE_STR = "true";
    public final static String FALSE_STR = "false";

    public static Object asObject(Object obj) {
        if (obj instanceof String) {
            String string = (String) obj;
            if (NumberUtil.isNumber(string)) {
                try {
                    return Integer.parseInt(string);
                } catch (NumberFormatException ignored) {
                }
                try {
                    return Long.parseLong(string);
                } catch (NumberFormatException ignored) {
                }
                try {
                    return Double.parseDouble(string);
                } catch (NumberFormatException ignored) {
                }
            }

            if (TRUE_STR.equalsIgnoreCase(string) || FALSE_STR.equalsIgnoreCase(string)) {
                return Boolean.valueOf(string);
            }
            return string;
        }
        return obj;
    }
}
