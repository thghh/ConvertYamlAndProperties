package com.github.chencn.yamlandprops.props2yaml;


import com.github.chencn.yamlandprops.yaml2props.Yaml2Props;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO
 *
 * @author pengzhikang
 * @date 2024/5/11 16:31
 * @since 1.0.0
 */
public class Props2YamlTest {

    @Test
    public void testFromMap() {
        Map<String, Object> configMap = new HashMap<>();
        configMap.put("server.port", 10023);
        configMap.put("spring.main.banner-mode", "log");
        configMap.put("spring.jpa.open-in-view", false);
        configMap.put("spring.data.mongodb.repositories.type", "none");
        configMap.put("spring.servlet.multipart.enabled", true);
        configMap.put("spring.servlet.multipart.file-size-threshold", 0);
        configMap.put("spring.servlet.multipart.max-file-size", 209715200);
        configMap.put("spring.servlet.multipart.max-request-size", 209715200);
        configMap.put("spring.datasource.core.driver-class-name", "com.mysql.jdbc.Driver");
        configMap.put("spring.datasource.core.jdbc-url", "jdbc:mysql://locahost:13306/test?useInformationSchema=true&?useUnicode=true&characterEncoding=utf8&useSSL=false&allowMultiQueries=true&allowPublicKeyRetrieval=true");
        configMap.put("spring.datasource.core.username", "root");
        configMap.put("spring.datasource.core.password", "root123");
        configMap.put("spring.datasource.core.hikari.maximum-pool-size", 20);
        configMap.put("spring.datasource.core.hikari.minimum-idle", 5);
        configMap.put("spring.datasource.hikari.connection-init-sql", "use test");
        configMap.put("oms.akka.port", 11200);
        configMap.put("oms.akka.http.port", 17300);
        configMap.put("oms.env", "DAILY");
        configMap.put("oms.instanceinfo.retention", 7);
        configMap.put("oms.container.retention.local", 1);
        configMap.put("oms.container.retention.remote", -1);
        configMap.put("oms.instance.metadata.cache.size", 1024);
        configMap.put("oms.accurate.select.server.percentage", 50);
        configMap.put("logging.config.classpath", "logback-dev.xml");
        configMap.put("table.name", Arrays.asList("name1", "name2"));
        configMap.put("table.sname", Arrays.asList("sname1", "sname2"));
        configMap.put("table.list", "text/html,text/css,application/javascript,application/json");

        Props2Yaml props2Yaml = Props2Yaml.fromMap(configMap);
        String convert = props2Yaml.convert();
        System.out.println(convert);

        Yaml2Props yaml2Props = Yaml2Props.fromContent(convert);
        Map<String, Object> stringObjectMap = yaml2Props.toMap();

        configMap.forEach((k, v) -> {
            Assert.assertEquals(v, stringObjectMap.get(k));
        });
    }

    @Test
    public void testFromFile() {
        URL resource = this.getClass().getResource("common-test.yml");
        Yaml2Props yaml2Props = Yaml2Props.fromFile(new File(resource.getFile()));
        String s = yaml2Props.toContent();
        System.out.println(s);
    }
}