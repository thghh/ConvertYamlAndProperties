# Convert Yaml And Properties Files

> You can quickly convert Properties files to YAML files, or YAML files to Properties files

[Gitee](https://gitee.com/thghh/ConvertYamlAndProperties)

## step 1
git clone https://gitee.com/thghh/ConvertYamlAndProperties.git

## step  2
git checkout -b jar

## step 3
mvn install

## step 4
```
    <dependency>
        <groupId>com.github.chenc.yamlandprops</groupId>
        <artifactId>ConvertYamlAndProperties</artifactId>
        <version>1.0.6</version>
    </dependency>
```


## Change Log
>1.0.6  Standalone jar mode.

>1.0.5  fix bugs.

>1.0.4  fix bugs.

>1.0.3  fix YAML file null value exception.

>1.0.2  Show menu only with files of specified type.